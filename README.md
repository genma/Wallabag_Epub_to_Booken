Ce projet est lié à mes billet de blogs :
* <a href="https://blog.genma.fr/?Lifehacking-Wallabag-Liseuse-et-faineantise-mon-projet-Billet-No1" title="Lifehacking - Wallabag, Liseuse et fainéantise : mon projet. Billet N°1">Lifehacking - Wallabag, Liseuse et fainéantise : mon projet. Billet N°1</a>
* Lifehacking - Wallabag, Liseuse et fainéantise : mon projet. Billet N°2

** L'idée est la suivante **
Avec une règle Udev, au branchement de la liseuse Booken en USB et à la détection de cette dernière, un script se lance. Ce script a pour tâches les actions suivantes :
* se connecter à Wallabag
* lancer l’export de l’epub
* copier l’epub sur la liseuse
* démonter proprement la liseuse

** Todo / RAF **
* Lire les paramètres (login, password, url) dans un fichier de configuration
* Mettre en "lu" dans Wallabag les entrées récupérées dans l’epub
* Renomer le fichier epub avec la date (par défaut il s’appelle unread.epub)
* Tout faire en python (et ce passer de la partie Shell)
* Optimiser le temps de timeout. A voir selon le nombre d'articles & le temps de connexion
* Ajouter des logs pour suivre la progression
* Envoyer un mail ou un SMS (utilisateur Freemobile) une fois la copie terminée
* Ajouter la gestion des dépendances python
* Ajouter des tests unitaires...
* ...
