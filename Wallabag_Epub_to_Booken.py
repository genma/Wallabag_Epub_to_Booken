#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import cookielib
import mechanize
import configparser
import time

# Lecture du fichier de param pour les codes
config = configparser.ConfigParser()
config.read('Config.ini')
configWallabag = config['WALLABAG']
LOGIN = configWallabag['LOGIN']
PASSWORD = configWallabag['PASSWORD']
WALLABAG_URL = configWallabag['WALLABAG_URL']
WALLABAG_EPUB_URL = configWallabag['WALLABAG_EPUB_URL']

# Date & time
now = time.localtime(time.time())

# Browser
br = mechanize.Browser()

# Enable cookie support for urllib2
cookiejar = cookielib.LWPCookieJar()
br.set_cookiejar(cookiejar)

# Browser options
br.set_handle_equiv(True)
br.set_handle_redirect(True)
br.set_handle_referer(True)
br.set_handle_robots(False)
br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

# authenticate
print (str(time.strftime("%d-%m-%y %H:%M:%S", now)) + " Lancement du script")
br.open(WALLABAG_URL)
br.select_form(name="loginform")
br['_username'] = LOGIN
br['_password'] = PASSWORD
br.submit()
print (str(time.strftime("%d-%m-%y %H:%M:%S", now)) + " Tentative de connexion à Wallabag")
url = br.open(WALLABAG_URL)
print (str(time.strftime("%d-%m-%y %H:%M:%S", now)) + " Connexion à Wallabag réussie")
# TODO  timeout value has to be optimized
url2 = br.open(WALLABAG_EPUB_URL, timeout=300.0)
print (str(time.strftime("%d-%m-%y %H:%M:%S", now)) +" Lancement de l'export de l'Epub")
now = time.localtime(time.time())
EpubFileName = "Wallabag_export_" + str(time.strftime("%d%m%y", now)) + ".epub"
with open(EpubFileName, 'w') as newsfobj:
    newsfobj.write(url2.read())
print (str(time.strftime("%d-%m-%y %H:%M:%S", now)) + " Epub généné & récupéré sous le nom " + EpubFileName)
